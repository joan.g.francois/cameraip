from flask import Flask, render_template, Response
import cv2
from flask_httpauth import HTTPBasicAuth

auth = HTTPBasicAuth()

app = Flask(__name__)

user = 'admin'
pw = 'mypassword'


def generate_password_hash(password):
    return password

users = {
    user: generate_password_hash(pw)
}

def check_password_hash(userPassword, password):
    return userPassword==password

def gen_frames():  # generate frame by frame from camera
    global camera
    while True:
        # Capture frame-by-frame
        success, frame = camera.read()  # read the camera frame
        #frame = cv2.rotate(frame, cv2.ROTATE_180)
        if not success:
            break
        else:
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result

@auth.verify_password
def verify_password(username, password):
    if username in users:
        return check_password_hash(users.get(username), password)
    return False

@app.route('/video_feed')
@auth.login_required
def video_feed():
    #Video streaming route. Put this in the src attribute of an img tag
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/')
@auth.login_required
def index():
    """Video streaming home page."""
    return render_template('index.html')


if __name__ == '__main__':
    camera = cv2.VideoCapture(0)
    app.run(host='0.0.0.0', debug=True, port=8080)